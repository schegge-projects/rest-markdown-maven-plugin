<#import 'macros.ftl' as m>
<#var info = description.info>
<#var servers = description.servers>
<#var pathStructure = description.pathStructure>
<#var schemas = description.components.schemas>
:icons: font
:fvicon:
:toc: macro
= ${info.title} <#if info.version??>(${info.version})</#if>
<#if (authors)??>
${authors}
</#if>
<#if (info.contact.email)??>

Contact: ${info.contact.email}
</#if>
<#if (info.description)??>

${info.description!}
</#if>

toc::[]
<#if (servers)??>

== Servers
[%header,cols="1l,2"]
|===
| URL | Description
<#list servers as server>
|${server.url}
|${server.description}
</#list>
|===
</#if>

<#list pathStructure as section, paths>

== ${section}
<#list paths as operation, endpoint>
<#if (endpoint.get)??>

<@m.EP operation="GET " + operation endpoint=endpoint.get/>
</#if>
<#if (endpoint.post)??>

<@m.EP operation="POST " + operation endpoint=endpoint.post/>
</#if>
<#if (endpoint.put)??>

<@m.EP operation="PUT " + operation endpoint=endpoint.put/>
</#if>
<#if (endpoint.delete)??>

<@m.EP operation="DELETE " + operation endpoint=endpoint.delete/>
</#if>
</#list>
</#list>

== Schemas
<#list schemas as key, schema>

=== ${key}

<#if (schema.hashValueType)??>
Hash of <<${schema.hashValueType.name}>>
<#elseif (schema.allOf)??>
extends <#list schema.allOf as allOf with l><#if (allOf.schema.name)??><<${(allOf.schema.name)!"missing"}>> </#if></#list>
</#if>
<#if (schema.properties)??>

[%header,cols="1l,1,1,1l,1l"]
|===
| Field Name | Required | Type | Format | Default
<#list schema.properties as propertyname, property>
|${propertyname}
|<#if (property.required)??>${property.required?then('icon:check[role="red"]','')}</#if>
<#if (property.schemas)??>
|<#if property.typeLabel == 'array'>array one of </#if>(<#list property.schemas as oneOf with l><<${oneOf.name}>><#if l?has_next>, </#if></#list>)
|
|
<#elseif (property.schema.name)??>
|<#if property.typeLabel == 'hash'>hash of <<${property.schema.hashValueType.name}>><#else><#if property.typeLabel == 'array'>array of </#if><<${property.schema.name!'missing'}>></#if>
|
|
<#else>
|${(property.typeLabel)!}
l|${(property.format)!}
|${(property.schema.defaultValue)!}
</#if>
</#list>
|===
</#if>
</#list>
