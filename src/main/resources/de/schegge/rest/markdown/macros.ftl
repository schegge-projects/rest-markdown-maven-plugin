<#macro EP operation endpoint>
=== <#if (endpoint.summary)??>${endpoint.summary}<#else>${operation}</#if>
<#if (endpoint.description)??>${endpoint.description}<#elseif (endpoint.summary)??>${endpoint.summary}.</#if>

<#if (endpoint.roles)??>
<#list endpoint.roles as role>
image:https://img.shields.io/badge/role-${role}-green[]
</#list>
</#if>

----
${operation}
----
<#if (endpoint.groupedParameters)??>

==== Parameters
<#list endpoint.groupedParameters as key, groupedParameters>

.${key}
[%header,cols="1l,1,2,1l,1l"]
|===
| Name | Required | Description | Type | Format
<#list groupedParameters as parameter>
|${parameter.name}
|<#if (parameter.required)??>${parameter.required?then('icon:check[role="red"]','')}</#if>
|<#if (parameter.description)??>${parameter.description}
<#elseif (parameter.schema.minimum)??>
must be greater than ${parameter.schema.minimum}
</#if>
|${parameter.schema.typeLabel}
|${parameter.schema.format!}
</#list>
|===
</#list>
</#if>
<#if (endpoint.requestBody)??>

==== Request Body <#if endpoint.requestBody.required>[role="red"]^required^</#if>
[%header,cols="1l,1"]

|===
| Content-Type | Schema
<#list endpoint.requestBody.content as type, content>
| ${type}
| <#if (content.schema.schema.name)??><<${content.schema.schema.name}>></#if>
</#list>
|===

</#if>

==== Responses

[%header,cols="1l,2l,2l,3"]
|===
| Code | Message | Content-Type | Schema
<#list endpoint.responses as code, response>
<#if (response.content)??>
<#list response.content as type, content>
| ${code}
| ${response.description}
| ${type}
| <#if (content.schema.schema.name)??><<${content.schema.schema.name}>></#if>
</#list>
<#else>
| ${code}
| ${response.description}
|
|
</#if>
</#list>
|===
</#macro>
