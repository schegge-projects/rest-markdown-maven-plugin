<#var info = description.info>
<#var servers = description.servers>
<#var pathStructure = description.pathStructure>
<#var schemas = description.components.schemas>
## ${info.title} <#if info.version??>(${info.version})</#if>
<#if (info.contact.email)??>
Contact: ${info.contact.email}
</#if>
${info.description!}

<#if (servers)??>
### Servers
| URL | Description |
| --- | ----------- |
<#list servers as server>
| ${server.url} | ${server.description} |
</#list>
</#if>

<#macro EP operation endpoint>
#### <#if (endpoint.summary)??>${endpoint.summary}<#else>${operation}</#if>
${endpoint.description!}

```
${operation}
```

<#if (endpoint.parameters)??>
#### Parameters
<#list endpoint.groupedParameters as key, groupedParameters>
##### ${key}
| Name | Required | Description | Type | Format |
| ---- | -------- | ----------- | ---- | ------ |
<#list groupedParameters as parameter>
| ${parameter.name} | <#if (parameter.required)??>${parameter.required?then('yes','')}</#if> | <#if (parameter.description)??>${parameter.description?replace("|", "\\|")} | <#elseif (parameter.schema.minimum)??> must be greater than ${parameter.schema.minimum} </#if> |${parameter.schema.typeLabel} |${parameter.schema.format!} |
</#list>
</#list>
</#if>

<#if (endpoint.requestBody)??>
##### Request Body <#if endpoint.requestBody.required>(required)</#if>
| Content-Type | Schema |
| ------------ | ------ |
<#list endpoint.requestBody.content as type, content>
| ${type} | <#if (content.schema.schema.name)??> [${content.schema.schema.name}](#${content.schema.schema.name?lower_case})</#if> |
</#list>
</#if>

##### Responses
| Code | Message | Content-Type | Schema |
| ---- | ------- | ------------ | ------ |
<#list endpoint.responses as code, response>
<#if (response.content)??>
<#list response.content as type, content>
| ${code}| ${response.description} | ${type}| <#if (content.schema.schema.name)??> [${content.schema.schema.name}](#${content.schema.schema.name?lower_case})</#if> |
</#list>
<#else>
| ${code} | ${response.description}| | |
</#if>
</#list>

</#macro>

<#list pathStructure as section, paths>
== ${section}

<#list paths as operation, endpoint>
<#if (endpoint.get)??>
<@EP operation="GET " + operation endpoint=endpoint.get/>
</#if>

<#if (endpoint.post)??>
<@EP operation="POST " +operation endpoint=endpoint.post/>
</#if>

<#if (endpoint.put)??>
<@EP operation="PUT " + operation endpoint=endpoint.put/>
</#if>

<#if (endpoint.delete)??>
<@EP operation="DELETE " + operation endpoint=endpoint.delete/>
</#if>
</#list>
</#list>

### Schemas

<#list schemas as key, schema>
#### ${key}

<#if (schema.hashValueType)??>
Hash of [${schema.hashValueType.name}](#${schema.hashValueType.name?lower_case})
</#if>
<#if (schema.properties)??>
| Field Name | Required | Type | Format | Default |
| ---------- | -------- | ---- | ------ | ------- |
<#list schema.properties as propertyname, property>
| ${propertyname} | <#if (property.required)??>${property.required?then('yes','')}</#if> | <#if (property.schema.name)??> | [${property.schema.name}](#${property.schema.name?lower_case}) | | <#else> | ${(property.typeLabel)!} |${(property.format)!} |${(property.schema.defaultValue)!} </#if> |
</#list>
</#if>
</#list>
