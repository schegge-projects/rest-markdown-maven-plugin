:icons: font
:fvicon:
= ${project.groupId}.${project.artifactId}: ${project.name} ${project.version}
<#if (project.developers)??><#list project.developers as d with l> ${d.name} <#if (d.email)??><${d.email}></#if><#if l?has_next>;</#if></#list></#if>

${project.description}

<#if (project.developers)??>
== Developers

|===
|Name | E-Mail | Roles

<#list project.developers as developer>
| ${developer.name}
| <${developer.email}>
| <#if (developer.roles)??><#list developer.roles as r with l>${r}<#if l?has_next>,</#if></#list></#if>
</#list>
|===
</#if>

<#if (project.organisation.name??)>
== ${project.organisation.name}

${project.organisation.url}
</#if>

<#if project.dependencies??>
== Dependencies

|===
| GroupId | ArtefactId | Version | Type | Scope
<#list project.dependencies as d>

| ${d.groupId}
| ${d.artifactId}
| ${d.version}
| ${d.type!}
| ${d.scope!}
</#list>
|===
</#if>