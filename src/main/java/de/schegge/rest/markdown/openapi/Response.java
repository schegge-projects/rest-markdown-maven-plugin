package de.schegge.rest.markdown.openapi;

import de.schegge.rest.markdown.ApiVisitor;

import java.util.Map;

public record Response(String description, Map<String, Content> content) {

    public <I> void accept(ApiVisitor<I> visitor, String code, I input) {
        visitor.visit(this, code, input);
    }
}

