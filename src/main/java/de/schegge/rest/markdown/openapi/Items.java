package de.schegge.rest.markdown.openapi;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record Items(@JsonProperty("$ref") String ref, List<Items> oneOf) {

}
