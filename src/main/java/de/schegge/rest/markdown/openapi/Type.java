package de.schegge.rest.markdown.openapi;

import static java.util.Objects.requireNonNull;

import java.util.Objects;
import java.util.stream.Stream;

public enum Type {
    UNKNOWN("unknown", null, null),
    INTEGER("integer", "integer", "int32"),
    INTEGER2("integer"),
    LONG("long", "integer", "int64"),
    FLOAT("number", "float"),
    DOUBLE("number", "double"),
    STRING("string"),
    URI("string", "uri"),
    BYTE("string", "byte"),
    BINARY("string", "binary"),
    DATE("string", "date"),
    DATE_TIME("string", "date-time"),
    PASSWORD("string", "password"),
    BOOLEAN("boolean", "boolean"),
    BOOLEAN2("boolean"),
    ARRAY("array"),
    HASH("hash"),
    OBJECT("object");

    private final String typeLabel;
    private final String format;
    private final String name;

    Type(String type) {
        this(type, type, null);
    }

    Type(String type, String format) {
        this(format, type, format);
    }

    Type(String name, String type, String format) {
        this.name = name;
        this.typeLabel = type;
        this.format = format;
    }

    public String getName() {
        return name;
    }

    public static Type by(Typed typed) {
        if (requireNonNull(typed).getTypeLabel() == null) {
            return UNKNOWN;
        }
        return Stream.of(values()).filter(p -> Objects.equals(p.typeLabel, typed.getTypeLabel()) && Objects.equals(p.format, typed.getFormat())).findFirst().orElse(UNKNOWN);
    }

    public String getFormat(String format, String defaultValue) {
        return Objects.equals(format, this.format) ? defaultValue : format;
    }
}
