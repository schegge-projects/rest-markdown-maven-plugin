package de.schegge.rest.markdown.openapi;

public record Info(String version, String title, String description, Contact contact) {

}
