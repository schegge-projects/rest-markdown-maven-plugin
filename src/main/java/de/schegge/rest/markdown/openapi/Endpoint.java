package de.schegge.rest.markdown.openapi;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.schegge.rest.markdown.ApiVisitor;

import java.util.List;
import java.util.TreeMap;

public final class Endpoint {

    private List<String> tags;
    @JsonProperty("x-roles")
    private List<String> roles;
    private String summary;
    private String description;
    private String operationId;
    private List<Parameter> parameters;
    private TreeMap<String, List<Parameter>> groupedParameters;
    private TreeMap<String, Response> responses;
    private RequestBody requestBody;

    public <I> Endpoint accept(ApiVisitor<I> visitor, String operation, I input) {
        return visitor.visit(this, operation, input);
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    public TreeMap<String, List<Parameter>> getGroupedParameters() {
        return groupedParameters;
    }

    public void setGroupedParameters(TreeMap<String, List<Parameter>> groupedParameters) {
        this.groupedParameters = groupedParameters;
    }

    public TreeMap<String, Response> getResponses() {
        return responses;
    }

    public void setResponses(TreeMap<String, Response> responses) {
        this.responses = responses;
    }

    public RequestBody getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(RequestBody requestBody) {
        this.requestBody = requestBody;
    }
}
