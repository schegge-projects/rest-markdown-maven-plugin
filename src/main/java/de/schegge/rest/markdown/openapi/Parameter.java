package de.schegge.rest.markdown.openapi;

import de.schegge.rest.markdown.ApiVisitor;

public class Parameter {

    private String name;
    private String in;
    private String description;
    private Boolean required;
    private ParameterSchema schema;

    public <I> void accept(ApiVisitor<I> visitor, I input) {
        visitor.visit(this, input);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIn() {
        return in;
    }

    public void setIn(String in) {
        this.in = in;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public ParameterSchema getSchema() {
        return schema;
    }

    public void setSchema(ParameterSchema schema) {
        this.schema = schema;
    }
}
