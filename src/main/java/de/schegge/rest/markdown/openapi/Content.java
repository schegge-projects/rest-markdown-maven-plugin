package de.schegge.rest.markdown.openapi;

import de.schegge.rest.markdown.ApiVisitor;

public record Content(ContentSchema schema) {

    public <I> void accept(ApiVisitor<I> visitor, String contentType, I input) {
        visitor.visit(this, contentType, input);
    }
}
