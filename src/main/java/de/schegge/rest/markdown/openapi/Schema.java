package de.schegge.rest.markdown.openapi;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.schegge.rest.markdown.ApiVisitor;

import java.util.List;
import java.util.Map;

public class Schema {

    @JsonIgnore
    private String name;

    private String type;

    private List<String> required;

    private List<Property> allOf;

    private List<Property> oneOf;

    private Map<String, Property> properties;

    private Map<String, String> additionalProperties;

    @JsonIgnore
    private Schema hashValueType;

    public <I> Schema accept(ApiVisitor<I> visitor, String name, I input) {
        return visitor.visit(this, name, input);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getRequired() {
        return required;
    }

    public void setRequired(List<String> required) {
        this.required = required;
    }

    public List<Property> getAllOf() {
        return allOf;
    }

    public void setAllOf(List<Property> allOf) {
        this.allOf = allOf;
    }

    public List<Property> getOneOf() {
        return oneOf;
    }

    public void setOneOf(List<Property> oneOf) {
        this.oneOf = oneOf;
    }

    public Map<String, Property> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, Property> properties) {
        this.properties = properties;
    }

    public Map<String, String> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, String> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public Schema getHashValueType() {
        return hashValueType;
    }

    public void setHashValueType(Schema hashValueType) {
        this.hashValueType = hashValueType;
    }
}
