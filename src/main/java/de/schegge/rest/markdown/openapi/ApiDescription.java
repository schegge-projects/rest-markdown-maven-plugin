package de.schegge.rest.markdown.openapi;

import java.util.LinkedHashMap;
import java.util.List;

public record ApiDescription(String openapi, Info info, List<Server> servers, LinkedHashMap<String, Path> paths, Components components) {

}
