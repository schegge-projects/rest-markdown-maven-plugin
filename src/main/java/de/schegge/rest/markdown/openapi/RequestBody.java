package de.schegge.rest.markdown.openapi;

import java.util.Map;

public record RequestBody(boolean required, Map<String, Content> content) {

}