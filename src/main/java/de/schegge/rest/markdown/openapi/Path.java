package de.schegge.rest.markdown.openapi;

import de.schegge.rest.markdown.ApiVisitor;

public record Path(Endpoint get, Endpoint post, Endpoint put, Endpoint delete) {

    public <I> void accept(ApiVisitor<I> visitor, String name, I input) {
        visitor.visit(this, name, input);
    }
}
