package de.schegge.rest.markdown.openapi;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.schegge.rest.markdown.ApiVisitor;

import java.util.List;

public class Property extends Typed {

    @JsonProperty("default")
    private String defaultValue;
    @JsonProperty("$ref")
    private String ref;
    private String pattern;
    private Items items;

    @JsonIgnore
    private List<Schema> schemas;
    @JsonIgnore
    private Schema schema;

    @JsonIgnore
    private boolean required;

    public <I> Property accept(ApiVisitor<I> visitor, String name, I input) {
        return visitor.visit(this, name, input);
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public Items getItems() {
        return items;
    }

    public void setItems(Items items) {
        this.items = items;
    }

    public List<Schema> getSchemas() {
        return schemas;
    }

    public void setSchemas(List<Schema> schemas) {
        this.schemas = schemas;
    }

    public Schema getSchema() {
        return schema;
    }

    public void setSchema(Schema schema) {
        this.schema = schema;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}
