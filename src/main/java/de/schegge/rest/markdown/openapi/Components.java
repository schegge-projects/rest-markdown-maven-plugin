package de.schegge.rest.markdown.openapi;

import de.schegge.rest.markdown.ApiVisitor;

import java.util.Map;

public record Components(Map<String, Schema> schemas) {

    public <I> Components accept(ApiVisitor<I> visitor, I input) {
        return visitor.visit(this, input);
    }
}
