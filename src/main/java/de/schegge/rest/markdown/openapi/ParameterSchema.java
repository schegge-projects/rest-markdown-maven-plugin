package de.schegge.rest.markdown.openapi;

public class ParameterSchema extends Typed {

    private Integer minimum;

    public Integer getMinimum() {
        return minimum;
    }

    public void setMinimum(Integer minimum) {
        this.minimum = minimum;
    }
}
