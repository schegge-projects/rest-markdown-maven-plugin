package de.schegge.rest.markdown.openapi;

public record Server(String url, String description) {

}
