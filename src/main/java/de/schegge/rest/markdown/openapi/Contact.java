package de.schegge.rest.markdown.openapi;

public record Contact(String name, String email) {

}
