package de.schegge.rest.markdown;

import de.schegge.rest.markdown.openapi.ApiDescription;
import de.schegge.rest.markdown.openapi.Components;
import de.schegge.rest.markdown.openapi.Content;
import de.schegge.rest.markdown.openapi.Endpoint;
import de.schegge.rest.markdown.openapi.Parameter;
import de.schegge.rest.markdown.openapi.Path;
import de.schegge.rest.markdown.openapi.Property;
import de.schegge.rest.markdown.openapi.Response;
import de.schegge.rest.markdown.openapi.Schema;

public interface ApiVisitor<I> {

  ApiDescription visit(ApiDescription apiDescription, I input);

  default Components visit(Components components, I input) {
    return components;
  }

  default Schema visit(Schema schema, String name, I input) {
    return schema;
  }

  default Property visit(Property property, String name, I input) {
    return property;
  }

  default Path visit(Path path, String name, I input) {
    return path;
  }

  default Endpoint visit(Endpoint endpoint, String operation, I input) {
    return endpoint;
  }

  default void visit(Content content, String contentType, I input) {

  }

  default void visit(Response response, String code, I input) {

  }

  default void visit(Parameter parameter, I input) {

  }
}
