package de.schegge.rest.markdown;

import de.schegge.rest.markdown.openapi.ApiDescription;
import de.schegge.rest.markdown.openapi.Components;
import de.schegge.rest.markdown.openapi.Property;
import de.schegge.rest.markdown.openapi.Schema;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import static java.util.stream.Collectors.toMap;

public class PropertySchemaUpdater implements ApiVisitor<Void> {

    private final Map<String, Schema> schemas;

    public PropertySchemaUpdater(Map<String, Schema> schemas) {
        this.schemas = schemas;
    }

    @Override
    public ApiDescription visit(ApiDescription apiDescription, Void input) {
        apiDescription.components().accept(this, input);
        return apiDescription;
    }

    @Override
    public Components visit(Components components, Void input) {
        Map<String, Schema> schemaMap = components.schemas();
        return new Components(schemaMap.entrySet().stream().collect(toMap(Entry::getKey, e -> e.getValue().accept(this, e.getKey(), null))));
    }

    @Override
    public Schema visit(Schema schema, String name, Void input) {
        List<Property> allOf = schema.getAllOf();
        if (allOf != null) {
            PropertyVisitor propertyVisitor = new PropertyVisitor(schemas, List.of());
            schema.setAllOf(allOf.stream().map(x -> x.accept(propertyVisitor, null, null)).toList());
        }
        List<String> required = Objects.requireNonNullElseGet(schema.getRequired(), List::of);
        Map<String, Property> properties = Objects.requireNonNullElseGet(schema.getProperties(), Map::of);
        PropertyVisitor propertyVisitor = new PropertyVisitor(schemas, required);
        properties.forEach((key, value) -> value.accept(propertyVisitor, key, null));
        return schema;
    }
}
