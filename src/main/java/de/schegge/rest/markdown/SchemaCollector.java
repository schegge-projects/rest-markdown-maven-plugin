package de.schegge.rest.markdown;

import de.schegge.rest.markdown.openapi.ApiDescription;
import de.schegge.rest.markdown.openapi.Components;
import de.schegge.rest.markdown.openapi.Schema;
import java.util.Map;

public class SchemaCollector implements ApiVisitor<Map<String, Schema>> {

  @Override
  public ApiDescription visit(ApiDescription apiDescription, Map<String, Schema> input) {
    apiDescription.components().accept(this, input);
    return apiDescription;
  }

  @Override
  public Components visit(Components components, Map<String, Schema> input) {
    components.schemas().forEach((key, value) -> value.accept(this, key, input));
    return components;
  }

  @Override
  public Schema visit(Schema schema, String name, Map<String, Schema> input) {
    schema.setName(name);
    input.put("#/components/schemas/" + name, schema);
    return schema;
  }
}
