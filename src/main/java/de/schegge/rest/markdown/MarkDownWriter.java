package de.schegge.rest.markdown;

import de.schegge.rest.markdown.formatter.StructuredApiDescription;
import de.schegge.rest.markdown.openapi.ApiDescription;
import de.schegge.rest.markdown.openapi.Endpoint;
import de.schegge.rest.markdown.openapi.Path;
import de.schegge.rest.markdown.openapi.Schema;
import org.freshmarker.Configuration;
import org.freshmarker.TemplateLoader;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Stream;

public class MarkDownWriter {

    private final Configuration cfg;

    public MarkDownWriter() {
        cfg = new Configuration();
        cfg.setTemplateLoader(new TemplateLoader() {
            @Override
            public String getImport(java.nio.file.Path path, String filename, Charset charset) throws IOException {
                try {
                    return Files.readString(java.nio.file.Path.of(getClass().getResource(filename).toURI()), charset);
                } catch (NullPointerException | URISyntaxException e) {
                    throw new IOException("import not found: " + filename);
                }
            }
        });
    }

    public void write(ApiDescription description, String type, PrintWriter writer) throws IOException {
        write(description, Map.of(), Map.of(), type, writer);
    }

    public void write(ApiDescription description, Map<String, Object> properties, Map<String, String> headerTags,
                      String type, PrintWriter writer) throws IOException {
        Map<String, Schema> schemaMap = new LinkedHashMap<>();
        new SchemaCollector().visit(description, schemaMap);
        schemaMap.values().forEach(schema -> {
            if (schema.getAdditionalProperties() != null) {
                schema.setType("hash");
                schema.setHashValueType(schemaMap.get(schema.getAdditionalProperties().get("$ref")));
            }
        });
        new PropertySchemaUpdater(schemaMap).visit(description, null);
        new EndPointUpdater(schemaMap).visit(description, null);
        Map<String, Object> data = new HashMap<>(properties);
        data.put("description", converted(description, headerTags));
        String fileName = type + ".ftl";
        try {
            cfg.builder().getTemplate(java.nio.file.Path.of(getClass().getResource(fileName).toURI())).process(data, writer);
        } catch (NullPointerException | URISyntaxException e) {
            throw new IOException("cannot read template: " + fileName, e);
        }
    }

    private StructuredApiDescription converted(ApiDescription description, Map<String, String> headerTags) {
        StructuredApiDescription converted = new StructuredApiDescription();
        converted.setServers(description.servers());
        converted.setComponents(description.components());
        converted.setInfo(description.info());
        converted.setOpenapi(description.openapi());

        Map<String, Map<String, Path>> structure = new LinkedHashMap<>();

        LinkedHashMap<String, Path> map = new LinkedHashMap<>();
        for (Entry<String, Path> entry : description.paths().entrySet().stream().sorted(Entry.comparingByKey()).toList()) {
            map.put(entry.getKey(), entry.getValue());
        }

        for (Entry<String, Path> pathEntry : map.entrySet()) {
            for (Endpoint endpoint : getEndpoints(pathEntry.getValue())) {
                String section = Stream.ofNullable(endpoint.getTags()).filter(Objects::nonNull).flatMap(List::stream)
                        .map(headerTags::get).filter(Objects::nonNull).findFirst().orElse("Endpoints");
                structure.computeIfAbsent(section, s -> new LinkedHashMap<>()).put(pathEntry.getKey(), pathEntry.getValue());
            }
        }
        converted.setPathStructure(structure);
        return converted;
    }

    private List<Endpoint> getEndpoints(Path path) {
        return Stream.of(path.get(), path.post(), path.put(), path.delete()).filter(Objects::nonNull).toList();
    }
}
