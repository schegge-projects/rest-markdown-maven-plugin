package de.schegge.rest.markdown.formatter;

import de.schegge.rest.markdown.openapi.Components;
import de.schegge.rest.markdown.openapi.Info;
import de.schegge.rest.markdown.openapi.Path;
import de.schegge.rest.markdown.openapi.Server;

import java.util.List;
import java.util.Map;

public class StructuredApiDescription {

    private String openapi;
    private Info info;
    private List<Server> servers;
    private Map<String, Map<String, Path>> pathStructure;
    private Components components;

    public String getOpenapi() {
        return openapi;
    }

    public void setOpenapi(String openapi) {
        this.openapi = openapi;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public List<Server> getServers() {
        return servers;
    }

    public void setServers(List<Server> servers) {
        this.servers = servers;
    }

    public Map<String, Map<String, Path>> getPathStructure() {
        return pathStructure;
    }

    public void setPathStructure(Map<String, Map<String, Path>> pathStructure) {
        this.pathStructure = pathStructure;
    }

    public Components getComponents() {
        return components;
    }

    public void setComponents(Components components) {
        this.components = components;
    }
}