package de.schegge.rest.markdown;

import de.schegge.rest.markdown.openapi.ApiDescription;
import de.schegge.rest.markdown.openapi.Items;
import de.schegge.rest.markdown.openapi.Property;
import de.schegge.rest.markdown.openapi.Schema;
import de.schegge.rest.markdown.openapi.Type;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

public class PropertyVisitor implements ApiVisitor<Void> {
    private final Map<String, Schema> schemas;
    private final List<String> required;

    public PropertyVisitor(Map<String, Schema> schemas, List<String> required) {
        this.schemas = schemas;
        this.required = required;
    }

    @Override
    public ApiDescription visit(ApiDescription apiDescription, Void input) {
        return null;
    }

    @Override
    public Property visit(Property property, String name, Void input) {
        if (name != null) {
            property.setRequired(required.contains(name));
        }
        Type type = Type.by(property);
        property.setType(type);
        property.setTypeLabel(type.getName());
        property.setFormat(type.getFormat(property.getFormat(), Optional.ofNullable(property.getPattern()).map(x -> x.replace("|", "\\|")).orElse(null)));
        Schema schema = schemas.get(property.getRef());
        property.setSchema(schema);
        if (schema != null && schema.getAdditionalProperties() != null) {
            property.setType(Type.HASH);
            property.setTypeLabel(Type.HASH.getName());
        }
        if (type != Type.ARRAY) {
            return property;
        }

        List<Items> oneOf = property.getItems().oneOf();
        if (oneOf == null) {
            property.setSchema(schemas.get(property.getItems().ref()));
            return property;
        }
        List<Schema> oneOfSchemas = oneOf.stream().map(x -> schemas.get(x.ref())).collect(toList());
        if (oneOfSchemas.size() == 1) {
            property.setSchema(oneOfSchemas.getFirst());
        } else {
            property.setSchemas(oneOfSchemas);
        }
        return property;
    }

}
