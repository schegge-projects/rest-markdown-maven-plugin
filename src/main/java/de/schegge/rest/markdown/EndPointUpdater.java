package de.schegge.rest.markdown;

import de.schegge.rest.markdown.openapi.ApiDescription;
import de.schegge.rest.markdown.openapi.Content;
import de.schegge.rest.markdown.openapi.Endpoint;
import de.schegge.rest.markdown.openapi.Parameter;
import de.schegge.rest.markdown.openapi.ParameterSchema;
import de.schegge.rest.markdown.openapi.Path;
import de.schegge.rest.markdown.openapi.RequestBody;
import de.schegge.rest.markdown.openapi.Response;
import de.schegge.rest.markdown.openapi.Schema;
import de.schegge.rest.markdown.openapi.Type;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class EndPointUpdater implements ApiVisitor<Void> {

    private final Map<String, Schema> schemas;

    public EndPointUpdater(Map<String, Schema> schemas) {
        this.schemas = schemas;
    }

    @Override
    public ApiDescription visit(ApiDescription apiDescription, Void input) {
        apiDescription.paths().forEach((k, v) -> v.accept(this, k, null));
        return apiDescription;
    }

    @Override
    public Path visit(Path path, String name, Void input) {
        return new Path(getEndpoint(path.get()), getEndpoint(path.post()), getEndpoint(path.put()), getEndpoint(path.delete()));
    }

    private Endpoint getEndpoint(Endpoint path) {
        return Optional.ofNullable(path).map(e -> e.accept(this, null, null)).orElse(null);
    }

    @Override
    public Endpoint visit(Endpoint endpoint, String operation, Void input) {
        List<Parameter> parameters = endpoint.getParameters();
        if (parameters != null) {
            parameters.forEach(p -> p.accept(this, input));
            endpoint.setGroupedParameters(parameters.stream().sorted(comparing(Parameter::getName))
                    .collect(groupingBy(Parameter::getIn, TreeMap::new, toList())));
        } else {
            endpoint.setGroupedParameters(new TreeMap<>());
        }
        List<String> roles = Objects.requireNonNullElseGet(endpoint.getRoles(), Collections::emptyList);
        endpoint.setRoles(roles.stream().map(x -> x.startsWith("ROLE_") ? x.substring(4) : x).map(x -> x.replace("-", "--"))
                .collect(toList()));

        RequestBody requestBody = endpoint.getRequestBody();
        if (requestBody != null) {
            requestBody.content().forEach((contentType, value) -> value.accept(this, contentType, input));
        }
        endpoint.getResponses().forEach((code, value) -> value.accept(this, code, input));
        return endpoint;
    }

    @Override
    public void visit(Response response, String code, Void input) {
        Map<String, Content> content = response.content();
        if (content != null) {
            content.forEach((contentType, value) -> value.accept(this, contentType, input));
        }
    }

    @Override
    public void visit(Content content, String contentType, Void input) {
        content.schema().setSchema(schemas.get(content.schema().getRef()));
    }

    @Override
    public void visit(Parameter parameter, Void input) {
        Optional.ofNullable(parameter.getDescription()).map(s -> s.replace("|", "\\|")).ifPresent(parameter::setDescription);
        ParameterSchema schema = parameter.getSchema();
        Type type = Type.by(schema);
        schema.setType(type);
        schema.setTypeLabel(type.getName());
        schema.setFormat(type.getFormat(schema.getFormat(), null));
    }
}
