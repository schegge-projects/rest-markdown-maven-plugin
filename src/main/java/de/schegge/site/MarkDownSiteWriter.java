package de.schegge.site;

import org.freshmarker.Configuration;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.Map;

public class MarkDownSiteWriter {

    private final Configuration cfg;

    public MarkDownSiteWriter() {
        cfg = new Configuration();
    }

    public void write(File template, Map<String, Object> properties, PrintWriter writer) throws IOException {
        if (template != null && template.canRead()) {
            cfg.builder().getTemplate(template.getName(), new FileReader(template)).process(properties, writer);
            return;
        }
        try {
            cfg.builder().getTemplate(java.nio.file.Path.of(getClass().getResource("site-asciidoc.ftl").toURI())).process(properties, writer);
        } catch (NullPointerException | URISyntaxException e) {
            throw new IOException("cannot read template: site-asciidoc.ftl");
        }
    }
}