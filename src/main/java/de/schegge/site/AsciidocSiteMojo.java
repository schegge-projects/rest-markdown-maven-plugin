package de.schegge.site;

import org.apache.maven.model.Model;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

/**
 * Generates a Asciidoc documentation from a maven project.
 */
@Mojo(name = "generate-site", threadSafe = true, defaultPhase = LifecyclePhase.PREPARE_PACKAGE)
public class AsciidocSiteMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    private MavenProject project;

    /**
     * The FreeMarker template file.
     */
    @Parameter
    private File template;

    /**
     * The output Asciidoc file.
     */
    @Parameter(defaultValue = "${project.build.directory}/README.adoc")
    private File output;

    /**
     * The character encoding.
     */
    @Parameter(defaultValue = "${project.build.sourceEncoding}")
    private String encoding;

    /**
     * Skip the generation of the documentation.
     */
    @Parameter
    boolean skip;

    public void execute() throws MojoExecutionException {
        if (skip) {
            getLog().info("Skip create markdown");
            return;
        }
        createMarkdown(template, project.getModel(), output.toPath(), getCharset());
    }

    private Charset getCharset() throws MojoExecutionException {
        try {
            return encoding == null ? StandardCharsets.UTF_8 : Charset.forName(encoding);
        } catch (RuntimeException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    private void createMarkdown(File template, Model model, Path outputPath, Charset charset) throws MojoExecutionException {
        getLog().info("create markdown " + model.getVersion());
        if (template != null && template.canRead()) {
            getLog().info("user template: " + template.getAbsolutePath());
        }
        try {
            Files.createDirectories(outputPath.getParent());
        } catch (IOException e) {
            getLog().warn("Cannot create folder: " + e.getMessage(), e);
            throw new MojoExecutionException("Cannot create folder: " + e.getMessage(), e);
        }
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(outputPath, charset))) {
            new MarkDownSiteWriter().write(template, Map.of("project", model), writer);
        } catch (RuntimeException | IOException e) {
            getLog().warn("Cannot write markdown: " + e.getMessage(), e);
            throw new MojoExecutionException("Cannot write markdown: " + e.getMessage(), e);
        } finally {
            getLog().info("finished markdown");
        }
    }
}