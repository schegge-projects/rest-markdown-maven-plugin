package de.schegge.restmarkdown.mojo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import de.schegge.collector.Filters;
import de.schegge.rest.markdown.MarkDownWriter;
import de.schegge.rest.markdown.openapi.ApiDescription;
import de.schegge.rest.markdown.openapi.Info;
import org.apache.maven.model.Contributor;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static java.util.stream.Collectors.joining;

/**
 * Generates a Asciidoc documentation from an OpenAPI JSON document.
 */
@Mojo(name = "generate-markdown", threadSafe = true, defaultPhase = LifecyclePhase.PREPARE_PACKAGE)
public class RestMarkdownMojo extends AbstractMojo {

  @Parameter(defaultValue = "${project}", required = true, readonly = true)
  private MavenProject project;

  /**
   * The alternative documentation title.
   */
  @Parameter
  private String title;

  /**
   * The alternative documentation version.
   */
  @Parameter
  private String version;

  /**
   * The output Asciidoc file.
   */
  @Parameter(defaultValue = "${project.build.directory}/openapi.adoc")
  private File output;

  /**
   * The input JSON file.
   */
  @Parameter(defaultValue = "${project.build.directory}/openapi.json")
  private File input;

  /**
   * The character encoding.
   */
  @Parameter(defaultValue = "${project.build.sourceEncoding}")
  private String encoding;

  @Parameter
  private List<Server> servers;

  @Parameter
  private Map<String, String> sections;

  /**
   * Skip the generation of the documentation.
   */
  @Parameter
  private boolean skip;

  /**
   * Force the generation of the documentation. Ignored on skip.
   */
  @Parameter
  private boolean force;

  public void execute() throws MojoExecutionException, MojoFailureException {
    if (skip) {
      getLog().info("Skip create markdown");
      return;
    }

    List<Server> valid = servers.stream().filter(Filters.nonNull(Server::getUrl))
        .filter(Filters.nonNull(Server::getDescription)).toList();
    if (servers.size() > valid.size()) {
      List<Server> invalid = new ArrayList<>(servers);
      invalid.removeAll(valid);
      throw new MojoExecutionException("invalid servers: " + invalid);
    }
    getLog().info("Servers: " + valid);

    Path inputPath = input.toPath();
    if (Files.notExists(inputPath)) {
      throw new MojoFailureException("Missing input: " + input);
    }

    Path outputPath = output.toPath();
    if (!force && Files.exists(outputPath) && isUpToDate(inputPath, outputPath)) {
      getLog().info("Nothing to convert: " + input + " older than " + output);
      return;
    }

    String authors = project.getModel().getDevelopers().stream().map(Contributor::getName).collect(joining("; "));
    getLog().info("Authors: " + authors);

    Charset charset = getCharset();
    ApiDescription description = getApiDescription(inputPath, charset);
    List<de.schegge.rest.markdown.openapi.Server> serverList = Objects.requireNonNullElseGet(description.servers(), ArrayList::new);
    for (Server server : servers) {
      serverList.add(createServer(server));
    }
    Optional<String> optionalTitle = Optional.ofNullable(title).filter(Predicate.not(String::isBlank));
    Optional<String> optionalVariable = Optional.ofNullable(version).filter(Predicate.not(String::isBlank));
    Info info = description.info();
    Info newInfo = new Info(optionalVariable.orElse(info.version()), optionalTitle.orElse(info.title()), info.description(), info.contact());
    ApiDescription newDescription = new ApiDescription(description.openapi(), newInfo, serverList, description.paths(), description.components());
    createMarkdown(newDescription, outputPath, authors, charset);
  }

  private de.schegge.rest.markdown.openapi.Server createServer(Server server) {
      return new de.schegge.rest.markdown.openapi.Server(Objects.toString(server.getUrl(), null), server.getDescription());
  }

  private boolean isUpToDate(Path inputPath, Path outputPath) throws MojoExecutionException {
    try {
      Instant lastModifiedInput = Files.getLastModifiedTime(inputPath).toInstant();
      Instant lastModifiedOutput = Files.getLastModifiedTime(outputPath).toInstant();
      return lastModifiedInput.isBefore(lastModifiedOutput);
    } catch (IOException e) {
      throw new MojoExecutionException(e.getMessage());
    }
  }

  private Charset getCharset() throws MojoExecutionException {
    try {
      return encoding == null ? StandardCharsets.UTF_8 : Charset.forName(encoding);
    } catch (RuntimeException e) {
      throw new MojoExecutionException(e.getMessage(), e);
    }
  }

  private void createMarkdown(ApiDescription description, Path outputPath, String authors,
      Charset charset) throws MojoExecutionException {
    getLog().info("create markdown");
    try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(outputPath, charset))) {
      new MarkDownWriter().write(description, Map.of("authors", authors), sections == null ? Map.of() : sections,
          "asciidoc", writer);
    } catch (IOException e) {
      throw new MojoExecutionException("Cannot write markdown: " + e.getMessage());
    }
    getLog().info("finished markdown");
  }

  private ApiDescription getApiDescription(Path inputPath, Charset charset) throws MojoExecutionException {
    try {
      String content = Files.readString(inputPath, charset);
      ObjectMapper mapper = content.startsWith("{") ? new ObjectMapper() : new ObjectMapper(new YAMLFactory());
      return mapper.configure(FAIL_ON_UNKNOWN_PROPERTIES, false).readValue(content, ApiDescription.class);
    } catch (IOException e) {
      throw new MojoExecutionException("Cannot read API description: " + e.getMessage());
    }
  }
}