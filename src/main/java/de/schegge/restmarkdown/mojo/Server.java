package de.schegge.restmarkdown.mojo;

import java.net.URI;

public class Server {

  private URI url;
  private String description;

  @Override
  public String toString() {
    return "Server{url=" + url + ", description='" + description + '\'' + '}';
  }

  public URI getUrl() {
    return url;
  }

  public void setUrl(URI url) {
    this.url = url;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
