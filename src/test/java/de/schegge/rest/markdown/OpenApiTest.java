package de.schegge.rest.markdown;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import de.schegge.rest.markdown.openapi.Schema;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class OpenApiTest {
    @Test
    void test() {
        String content = """
                {
                  "required" : [ "abbreviation", "name", "shortName" ],
                  "type" : "object",
                  "allOf" : [ {
                    "$ref" : "#/components/schemas/MarketRoleDto"
                  }, {
                    "type" : "object",
                    "properties" : {
                      "ticketSystemConfig" : {
                        "$ref" : "#/components/schemas/SupplierTicketSystemConfigDto"
                      }
                    }
                  } ]
                },
                """;
        assertThrows(UnrecognizedPropertyException.class, () ->new ObjectMapper().readValue(content, Schema.class),
                "the model is to simple to detect this kind of schema declaration");
    }
}
