package de.schegge.rest.markdown;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.schegge.rest.markdown.openapi.ApiDescription;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

class AsciiDocTest {
    @Test
    void checkAsciidoc() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        InputStream stream = getClass().getResourceAsStream("/test.json");
        ApiDescription description = objectMapper.readValue(stream, ApiDescription.class);
        try (StringWriter out = new StringWriter(); PrintWriter writer = new PrintWriter(out)) {
            Map<String, String> headerTags = Map.of("header1", "Header 1", "header2", "Header 2");
            new MarkDownWriter().write(description, Map.of(), headerTags, "asciidoc", writer);
            Assertions.assertEquals(
                    """
                            :icons: font
                            :fvicon:
                            :toc: macro
                            = OpenAPI definition (v1)

                            Contact: developer@schegge.de

                            toc::[]

                            == Servers
                            [%header,cols="1l,2"]
                            |===
                            | URL | Description
                            |http://localhost
                            |Local test server
                            |===


                            == Endpoints

                            === GET /download



                            ----
                            GET /download
                            ----

                            ==== Parameters

                            ==== Responses
                            
                            [%header,cols="1l,2l,2l,3"]
                            |===
                            | Code | Message | Content-Type | Schema
                            | 200
                            | OK
                            | */*
                            | <<StreamingResponseBody>>
                            |===

                            === Get organisations
                            Get a list of organisations

                            image:https://img.shields.io/badge/role-administrator-green[]

                            ----
                            GET /organisations
                            ----

                            ==== Parameters

                            ==== Responses

                            [%header,cols="1l,2l,2l,3"]
                            |===
                            | Code | Message | Content-Type | Schema
                            | 200
                            | OK
                            | */*
                            | <<CollectionModelEntityModelOrganisationDto>>
                            |===

                            === Get an organisation
                            Get a single organisation

                            image:https://img.shields.io/badge/role-member-green[]
                            image:https://img.shields.io/badge/role-administrator-green[]

                            ----
                            GET /organisations/{id}
                            ----

                            ==== Parameters
                            
                            .path
                            [%header,cols="1l,1,2,1l,1l"]
                            |===
                            | Name | Required | Description | Type | Format
                            |id
                            |icon:check[role="red"]
                            ||long
                            |
                            |===

                            ==== Responses

                            [%header,cols="1l,2l,2l,3"]
                            |===
                            | Code | Message | Content-Type | Schema
                            | 200
                            | OK
                            | */*
                            | <<EntityModelOrganisation>>
                            |===

                            == Schemas

                            === CollectionModelEntityModelOrganisationDto


                            [%header,cols="1l,1,1,1l,1l"]
                            |===
                            | Field Name | Required | Type | Format | Default
                            |_embedded
                            |
                            |object
                            l|
                            |
                            |_links
                            |
                            |hash of <<Link>>
                            |
                            |
                            |===
                            
                            === EntityModelOrganisationDto


                            [%header,cols="1l,1,1,1l,1l"]
                            |===
                            | Field Name | Required | Type | Format | Default
                            |name
                            |
                            |string
                            l|
                            |
                            |category
                            |
                            |string
                            l|[a-z-=\\|]
                            |
                            |_links
                            |
                            |hash of <<Link>>
                            |
                            |
                            |===
                            
                            === Links

                            Hash of <<Link>>

                            === EntityModelOrganisation


                            [%header,cols="1l,1,1,1l,1l"]
                            |===
                            | Field Name | Required | Type | Format | Default
                            |name
                            |
                            |string
                            l|
                            |
                            |createdDate
                            |
                            |date-time
                            l|
                            |
                            |modifiedDate
                            |
                            |date-time
                            l|
                            |
                            |_links
                            |
                            |hash of <<Link>>
                            |
                            |
                            |===
                            
                            === StreamingResponseBody


                            === Link


                            [%header,cols="1l,1,1,1l,1l"]
                            |===
                            | Field Name | Required | Type | Format | Default
                            |href
                            |
                            |string
                            l|
                            |
                            |hreflang
                            |
                            |string
                            l|
                            |
                            |title
                            |
                            |string
                            l|
                            |
                            |type
                            |
                            |string
                            l|
                            |
                            |deprecation
                            |
                            |string
                            l|
                            |
                            |profile
                            |
                            |string
                            l|
                            |
                            |name
                            |
                            |string
                            l|
                            |
                            |templated
                            |
                            |boolean
                            l|
                            |
                            |===
                            """,
                    out.toString().replaceAll("\r", ""));
        }
    }

    @Test
    void createMarkdown() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        InputStream stream = getClass().getResourceAsStream("/test.json");
        ApiDescription description = objectMapper.readValue(stream, ApiDescription.class);
        try (PrintWriter writer = new PrintWriter(new FileWriter("target/response2.md"))) {
            new MarkDownWriter().write(description, "markdown", writer);
        }
    }

    @Test
    void createAsciidoc2() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        InputStream stream = getClass().getResourceAsStream("/test.json");
        ApiDescription description = objectMapper.readValue(stream, ApiDescription.class);
        try (PrintWriter writer = new PrintWriter(new FileWriter("target/response2.adoc"))) {
            new MarkDownWriter().write(description, "asciidoc", writer);
        }
    }

    @Test
    void createAsciidoc3() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        InputStream stream = getClass().getResourceAsStream("/openapi.json");
        ApiDescription description = objectMapper.readValue(stream, ApiDescription.class);
        try (PrintWriter writer = new PrintWriter(new FileWriter("target/response3.adoc"))) {
            new MarkDownWriter().write(description, "asciidoc", writer);
        }
    }
}
